dotfiles
========

My dotfiles. Install script borrowed heavily from [homebrew](http://brew.sh/)

Install
=======
`ruby -e "$(curl -fsSL https://raw.github.com/davidkariuki/dotfiles/go/install)"`

